﻿using UnityEngine;
using System.Collections;

public class SceneFadeInOut : MonoBehaviour
{
	public float fadeSpeed = 1.5f;          // Speed that the screen fades to and from black.
	
	
	private bool sceneStarting = true;      // Whether or not the scene is still fading in.

	private bool isEnded = false;
	
	void Awake ()
	{
		// Set the texture so that it is the the size of the screen and covers it.
		this.guiTexture.pixelInset = new Rect(0f, 0f, Screen.width, Screen.height);
	}
	
	
	void Update ()
	{
		// If the scene is starting...
//		for(int i =0; i < 500000;i++)
//		{
//
//		}
		if(sceneStarting)
		{	// ... call the StartScene function.
			StartScene();
		}
		else if(isEnded == false)
			EndScene();
	}
	
	
	void FadeToClear ()
	{
		// Lerp the colour of the texture between itself and transparent.
		this.guiTexture.color = Color.Lerp(this.guiTexture.color, Color.clear, fadeSpeed * Time.deltaTime);
	}
	
	
	void FadeToBlack ()
	{
		// Lerp the colour of the texture between itself and black.
		this.guiTexture.color = Color.Lerp(this.guiTexture.color, Color.black, fadeSpeed * Time.deltaTime);
	}
	
	
	void StartScene ()
	{
		// Fade the texture to clear.
		FadeToClear();
		
		// If the texture is almost clear...
		if(this.guiTexture.color.a <= 0.05f)
		{
			// ... set the colour to clear and disable the GUITexture.
			guiTexture.color = Color.clear;
			guiTexture.enabled = false;
			
			// The scene is no longer starting.
			sceneStarting = false;
		}
	}
	
	
	public void EndScene ()
	{
		// Make sure the texture is enabled.
		this.guiTexture.enabled = true;
		
		// Start fading towards black.
		FadeToBlack();
//		Debug.Log("------------> is turn new scene");
		// If the screen is almost black...
		if(this.guiTexture.color.a >= 0.95f)
		{
			// ... reload the level.
			isEnded = true;
//		    int curLevel = Application.loadedLevel;
			Debug.Log("------------> is turn new scene : " + Application.loadedLevelName);
			// is intro screen
			if(Application.loadedLevelName.Equals("introScene"))
			{
				// load login screen
				Application.LoadLevel("loginScene");
			}
		}
	}
}