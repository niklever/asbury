﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour {

	/*For Menu Item Panel*/

	public UnityEngine.UI.Button btnMenu;
	public UnityEngine.UI.Button btnMenuCoaching;
	public UnityEngine.UI.Button btnMenuApple;
	public UnityEngine.UI.Button btnMenuResource;

	/*For Englarge Award Item Panel*/
	public UnityEngine.UI.Button btnEnglargeAwardItem;
	public UnityEngine.UI.Text txtAwardNote;
	public UnityEngine.UI.Image englargeAwardItemBox;

	List<BadgeAwardItem> lstAwards;
	List<Student> lstStudentResource;

	/*For Resource Panel*/
	public UnityEngine.UI.Text leftNameLbl;
	public UnityEngine.UI.Text leftName;
	public UnityEngine.UI.Text leftDescription;
	public UnityEngine.UI.Button leftAvatar;

	public UnityEngine.UI.Text rightNameLbl;
	public UnityEngine.UI.Text rightName;
	public UnityEngine.UI.Text rightDescription;
	public UnityEngine.UI.Button rightAvatar;
	public UnityEngine.UI.Image resourceBox;

	bool showResource,showEnglargeAwardItem,showBadgeAwards;
	int currentStudentIdx;

	Sprite menu_plus,menu_minus,
	menu_coaching,menu_apple,menu_resource,menu_empty
		,menu_coaching_highlight,menu_apple_highlight,menu_resource_highlight;

	// Use this for initialization
	bool isExtended = false;
	bool shouldExtended = false;
	bool updated = true;

	void Start () {
		/* init Awards List*/
		lstAwards = new List<BadgeAwardItem>();
		lstAwards.Add(new BadgeAwardItem( BadgeAward.APPLE,"BadgeAward.APPLE"));
		lstAwards.Add(new BadgeAwardItem( BadgeAward.PENCIL,"BadgeAward.PENCIL"));
		lstAwards.Add(new BadgeAwardItem( BadgeAward.HEART,"BadgeAward.HEART"));
		lstAwards.Add(new BadgeAwardItem( BadgeAward.TEACHER_1,"BadgeAward.TEACHER_1"));
		lstAwards.Add(new BadgeAwardItem( BadgeAward.APPLE,"BadgeAward.APPLE"));

		/*init Resource List*/
		currentStudentIdx = 0;
		lstStudentResource = new List<Student>();
		lstStudentResource.Add(new Student("abc",null, "Student 1"));
		lstStudentResource.Add(new Student("abc",null, "Student 2"));
		lstStudentResource.Add(new Student("abc",null, "Student 3"));
		lstStudentResource.Add(new Student("abc",null, "Student 4"));
		lstStudentResource.Add(new Student("abc",null, "Student 5"));

		menu_plus = Sprite.Create( (Texture2D)(Resources.Load ("images/menu_plus")) , new Rect(0,0,162,130), new Vector2(0,0));
		menu_minus = Sprite.Create( (Texture2D)(Resources.Load ("images/menu_minus")) , new Rect(0,0,164,128), new Vector2(0,0));

		btnMenuCoaching.GetComponent<RectTransform>().sizeDelta = new Vector2(192/2,92/2);
		btnMenuApple.GetComponent<RectTransform>().sizeDelta = new Vector2(168/2,86/2);
		btnMenuResource.GetComponent<RectTransform>().sizeDelta =new Vector2(222/2,104/2);

		menu_coaching = Sprite.Create( (Texture2D)(Resources.Load ("images/Menu_Coaching")) , new Rect(0,0,192,92), new Vector2(0,0));
		menu_apple = Sprite.Create( (Texture2D)(Resources.Load ("images/Menu_Apple")) , new Rect(0,0,168,86), new Vector2(0,0));
		menu_resource = Sprite.Create( (Texture2D)(Resources.Load ("images/Menu_Resource")) , new Rect(0,0,202,104), new Vector2(0,0));

		menu_coaching_highlight = Sprite.Create( (Texture2D)(Resources.Load ("images/Menu_Coaching_hightlight")) , new Rect(0,0,192,92), new Vector2(0,0));
		menu_apple_highlight = Sprite.Create( (Texture2D)(Resources.Load ("images/Menu_Apple_hightlight")) , new Rect(0,0,168,86), new Vector2(0,0));
		menu_resource_highlight = Sprite.Create( (Texture2D)(Resources.Load ("images/Menu_Resource_hightlight")) , new Rect(0,0,222,104), new Vector2(0,0));

		menu_empty = Sprite.Create( (Texture2D)(Resources.Load ("images/bg_empty")) , new Rect(0,0,81,100), new Vector2(0,0));


		btnMenu.image.sprite = menu_plus;
		btnMenuCoaching.interactable = false;
		btnMenuApple.interactable = false;
		btnMenuResource.interactable = false;
		btnMenuCoaching.image.sprite = menu_empty;
		btnMenuResource.image.sprite = menu_empty;
		btnMenuApple.image.sprite = menu_empty;
	}
	
	
	// Update is called once per frame
	void Update () {



	}

	
	void showBadgeAWards(){
		//backup color 
		Color backupColor = GUI.color;
		Color backupContentColor = GUI.contentColor;
		Color backupBackgroundColor = GUI.backgroundColor;

		GUI.Box(new Rect( 0,0,1024, 768),(Texture2D)(Resources.Load("images/blank_background")));
		
		GUI.backgroundColor = Color.clear;
		Rect bounds = new Rect( 32,84,960, 600);
		GUI.Box(bounds,(Texture2D)(Resources.Load("images/badges_bg")));
		
		bounds = new Rect( 892,94,60, 40);
		if(GUI.Button(bounds,""))
		{
			Debug.Log("Click on Close");
			showBadgeAwards = false;
		}
		
		int step = 230;
		
		for(int i = 0; i < lstAwards.Count; i ++)
		{
			BadgeAwardItem awardItem = lstAwards[i];
			bounds = new Rect( 87 + (i%4)*step, 210 + (i/4)*step ,150, 150);
			string imgPath = "";
			switch(awardItem.type)
			{
			case BadgeAward.APPLE:
				imgPath = "images/AppleAward";
				break;
			case BadgeAward.DISTRICT_AWARD:
				imgPath = "images/District Award";
				break;
			case BadgeAward.FLOWER:
				imgPath = "images/Flower";
				break;
			case BadgeAward.HEART:
				imgPath = "images/Heart";
				break;
			case BadgeAward.PENCIL:
				imgPath = "images/Pencil";
				break;
			case BadgeAward.STAR:
				imgPath = "images/Star";
				break;
			case BadgeAward.TEACHER_1:
				imgPath = "images/teacher!";
				break;
			case BadgeAward.THANKYOUNOTE:
				imgPath = "images/ThankyouNote";
				break;
			default:
				imgPath = "images/AppleAward";
				break;
			}
			Texture2D imgTexture = (Texture2D)(Resources.Load (imgPath));
			if(GUI.Button(bounds,imgTexture))
			{
				Debug.Log("Click on Badge");
				showEnglargeAwardItem = true;
				showBadgeAwards = false;
				englargeAwardItemBox.gameObject.SetActive(true);

				btnEnglargeAwardItem.image.sprite = Sprite.Create( imgTexture , new Rect(0,0,imgTexture.width,imgTexture.height), new Vector2(0,0));
				txtAwardNote.text = awardItem.note;
			}
		}
		
		//Reset color
		GUI.color = backupColor;
		GUI.contentColor = backupContentColor;
		GUI.backgroundColor = backupBackgroundColor;
	}
	void OnGUI() {
		// show badge awards
		GUI.depth = 1000;
		if(showBadgeAwards)
		{
			showBadgeAWards();

		}

		if(shouldExtended && isExtended == false && updated == false)
		{
			isExtended = true;
			shouldExtended = false;
			btnMenu.image.sprite = menu_minus;

			btnMenuCoaching.image.sprite = menu_coaching;
			btnMenuApple.image.sprite = menu_apple;
			btnMenuResource.image.sprite = menu_resource;
			btnMenuCoaching.interactable = true;
			btnMenuApple.interactable = true;
			btnMenuResource.interactable = true;
			btnMenuCoaching.gameObject.SetActive(true);
			btnMenuApple.gameObject.SetActive(true);
			btnMenuResource.gameObject.SetActive(true);

		}
		else if(shouldExtended == false && isExtended == true&& updated == false)
		{
			btnMenu.image.sprite = menu_plus;
			isExtended = false;
			shouldExtended = false;
			btnMenuCoaching.interactable = false;
			btnMenuApple.interactable = false;
			btnMenuResource.interactable = false;
			btnMenuCoaching.image.sprite = menu_empty;
			btnMenuResource.image.sprite = menu_empty;
			btnMenuApple.image.sprite = menu_empty;
			btnMenuCoaching.gameObject.SetActive(false);
			btnMenuApple.gameObject.SetActive(false);
			btnMenuResource.gameObject.SetActive(false);
		}
		updated = true;
	}



	public void menuButtonClick(){
		if(!showBadgeAwards)
		{
			Debug.Log("Handle Click on Menu");
			if(isExtended)
			{
				shouldExtended = false;

			}else {
				btnMenuCoaching.GetComponent<RectTransform>().sizeDelta = new Vector2(192/2,92/2);
				btnMenuApple.GetComponent<RectTransform>().sizeDelta = new Vector2(168/2,86/2);
				btnMenuResource.GetComponent<RectTransform>().sizeDelta =new Vector2(222/2,104/2);
				shouldExtended = true;

			}
			updated = false;
		}

	}
	

	public void CoachingMenuButtonClick(){
		if(!showBadgeAwards)
		{
			Debug.Log("Handle CoachingMenuButtonClick");
			
//			btnMenuCoaching.image.sprite = menu_coaching_highlight;
//			btnMenuApple.image.sprite = menu_apple;
//			btnMenuResource.image.sprite = menu_resource;
//			btnMenuCoaching.GetComponent<RectTransform>().sizeDelta = new Vector2(192/2,92/2);
//			btnMenuApple.GetComponent<RectTransform>().sizeDelta = new Vector2(168/2,86/2);
//			btnMenuResource.GetComponent<RectTransform>().sizeDelta =new Vector2(222/2,104/2);
		}

	}

	public void AppleBadgesMenuButtonClick(){
		if(!showBadgeAwards)
		{
			Debug.Log("Handle AppleBadgesMenuButtonClick");
			
//			btnMenuCoaching.image.sprite = menu_coaching;
//			btnMenuApple.image.sprite = menu_apple_highlight;
//			btnMenuResource.image.sprite = menu_resource;
//			btnMenuCoaching.GetComponent<RectTransform>().sizeDelta = new Vector2(192/2,92/2);
//			btnMenuApple.GetComponent<RectTransform>().sizeDelta = new Vector2(168/2,86/2);
//			btnMenuResource.GetComponent<RectTransform>().sizeDelta =new Vector2(222/2,104/2);
		}

		showBadgeAwards = true;
		showResource = false;
	}

	public void ResourceMenuButtonClick(){
		if(!showBadgeAwards)
		{
			Debug.Log("Handle ResourceMenuButtonClick");
			
//			btnMenuCoaching.image.sprite = menu_coaching;
//			btnMenuApple.image.sprite = menu_apple;
//			btnMenuResource.image.sprite = menu_resource_highlight;
//			btnMenuCoaching.GetComponent<RectTransform>().sizeDelta = new Vector2(192/2,92/2);
//			btnMenuApple.GetComponent<RectTransform>().sizeDelta = new Vector2(168/2,86/2);
//			btnMenuResource.GetComponent<RectTransform>().sizeDelta = new Vector2(222/2,104/2);
		}

		showBadgeAwards = false;
		showResource = true;
		resourceBox.gameObject.SetActive(true);
		UpdateStudentInfoBox();
	}

	public void moveLeftResourceButtonClick(){
		if(showResource)
		{
			if(currentStudentIdx == 0)
			{
				return;
			}
			currentStudentIdx -=2;
			UpdateStudentInfoBox();


		}
	}

	public void moveRightResourceButtonClick(){
		if(showResource)
		{
			if(currentStudentIdx >= lstStudentResource.Count -2)
			{
				return;
			}

			currentStudentIdx +=2;
			UpdateStudentInfoBox();

		}
	}
	public void loadRemoteImage(Student std,Button btn)
	{
		if(std!= null && std.avatar != null)
		{
			CustomTextureLoader ImageLoader = new CustomTextureLoader
			{
				Cache = false,
			}; // loads textures
			string _imgUrl = Base.BASE_URL + "images/" + std.avatar;
			ImageLoader.LoadImage(_imgUrl, delegate(Texture texture)
			                      
			                      {
				Texture2D avaTexture = null;
				//				this._shouldShowLoading = false;
				if(texture == null)
				{
					
					avaTexture = (Texture2D)Resources.Load("images/temp/no_image");
					
				}else
				{
					avaTexture = (Texture2D)texture;
				}
				
				btn.image.sprite = Sprite.Create( avaTexture , new Rect(0,0,avaTexture.width,avaTexture.height), new Vector2(0,0));
				
			});
		}else {
			btn.image.sprite = Sprite.Create( (Texture2D)Resources.Load("images/temp/no_image") , new Rect(0,0,150,200), new Vector2(0,0));
		}


	}

	public void UpdateStudentInfoBox(){
		Student leftStd= null;
		Student rightStd = null;
		if(lstStudentResource.Count > 0)
		{
			leftStd= lstStudentResource[currentStudentIdx];
			if(currentStudentIdx + 1 <= lstStudentResource.Count -1)
			{
				rightStd = lstStudentResource[currentStudentIdx + 1];
			}
		}
		
		if(leftStd != null)
		{
			leftNameLbl.text = "Name:";
			leftName.text = leftStd.name;
			leftDescription.text = leftStd.description;
			loadRemoteImage(leftStd,leftAvatar);

			
		}else {
			leftNameLbl.text = "";
			leftName.text = "";
			leftDescription.text = "";
			rightAvatar.image.sprite = menu_empty;
		}
		
		if(rightStd != null)
		{
			rightNameLbl.text = "Name:";
			rightName.text = rightStd.name;
			rightDescription.text = rightStd.description;
			loadRemoteImage(rightStd,rightAvatar);
			
		}else {
			rightNameLbl.text = "";
			rightName.text = "";
			rightDescription.text = "";
			rightAvatar.image.sprite = menu_empty;
		}
	}

	public void closeResourceButtonClick(){
		if(showResource)
		{
			showResource= false;
			resourceBox.gameObject.SetActive(false);
		}
	}

	public void closeEnlargeAwardItemButtonClick(){
		if(showEnglargeAwardItem)
		{
			showEnglargeAwardItem= false;
			showBadgeAwards = true;
			englargeAwardItemBox.gameObject.SetActive(false);
		}
	}
	
	public void PrintAwardItemButtonClick(){
		if(showEnglargeAwardItem)
		{
			Debug.Log("Handle Print Award Item Action!!!");
		}
	}

}
