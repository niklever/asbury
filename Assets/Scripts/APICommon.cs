﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml;
using System.Xml.Serialization;
using SimpleJSON;

using UnityEngine;

using eDriven.Core.Events;
using eDriven.Gui;
using eDriven.Gui.Components;
using eDriven.Gui.Containers;
using eDriven.Gui.Cursor;
using eDriven.Gui.Layout;
using eDriven.Gui.Form;
using eDriven.Gui.Styles;
using eDriven.Gui.Events;
using eDriven.Gui.Plugins;
using eDriven.Networking.Rpc;
using System.Text.RegularExpressions;

using Event = eDriven.Core.Events.Event;
using Debug = UnityEngine.Debug;
using JsonFx.Json;
using System.IO;
using Assets.Scripts.GUI.WebAPIModels;

enum Platform { EDITOR, IPAD, RETINA, ANDROID, WEBPLAYER };



public class APICommon : Gui {

	private static string serverPath = "http://adbury.gkxim.com";

	public static APICommon apiCommon;
	private readonly HttpConnector _httpConnector = new HttpConnector { ResponseMode = ResponseMode.WWW };

	public static string ServerPath
	{
		get
		{
			return serverPath;
		}
	}

}//end of script
