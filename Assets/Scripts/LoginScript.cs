﻿using UnityEngine;
using System.Collections;
using eDriven.Gui.Components;
using Assets.Scripts.GUI.WebAPIModels;
//using UnityEditor;

public class LoginScript : MonoBehaviour {

	public float fadeSpeed = 1.5f;          // Speed that the screen fades to and from black.
	
	public UnityEngine.UI.Image FadeOutImg;
	
	private bool isEnded = false;

	private bool shouldFadeOut = false;

	public UnityEngine.UI.InputField userName;
	public UnityEngine.UI.InputField passWord;
	public UnityEngine.UI.Button submit_Buton;
	private string message;
	private bool showDialog;
	bool isTouchOn;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update ()
	{
		if(shouldFadeOut && !isEnded)
		{
			FadeOut();
		}
	}

	public void onUserNameTextValueChange(){

		Debug.Log ("onUserNameTextValueChange "+ userName.text);
	}

	public void onPassWordTextValueChange(){
		Debug.Log ("onPassWordTextValueChange "+ passWord.text);
	}

	public void onSubmitButtonClick()
	{
		Debug.Log ("onSubmitButtonClick " + userName.text + " , " + passWord.text);

		CreateAccountModel accountModel = new CreateAccountModel()
		{
			UserName = userName.text,
			Password = passWord.text,
			FirstName = "???",
			LastName = "???",
		};

		Debug.Log ("Handle AccountModel  " + accountModel.FirstName);
		LevelScript.username = accountModel.UserName;

		showDialog = false;
		// fadeout and show fist game scenario
		FadeOutImg.gameObject.SetActive(true);
		FadeOutImg.color = Color.clear;
		shouldFadeOut = true;
	}

	/* show custom dialog*/


	public string stringToEdit = "Hello World\nI've got 2 lines...";
	void OnGUI() 
	{
		if(showDialog)
		{
			GUI.enabled = false;
			//backup color 
			Color backupColor = GUI.color;
			Color backupContentColor = GUI.contentColor;
			Color backupBackgroundColor = GUI.backgroundColor;
			
			//add textarea with transparent text
			GUI.skin.textArea.normal.background = (Texture2D)(Resources.Load("images/dialogue"));
			GUI.skin.textArea.hover.background = (Texture2D)(Resources.Load("images/dialogue"));


			GUI.contentColor = new Color(1f, 1f, 1f, 0f);
			GUIStyle style = new GUIStyle(GUI.skin.textArea);
			Rect bounds = new Rect( Screen.width - 570,100,570, 80);
			stringToEdit = GUI.TextArea(bounds, stringToEdit);
//			GUI.Button(new Rect(10, 20,100,100), "stringToEdit");



			//get the texteditor of the textarea to control selection
			int controlID = GUIUtility.GetControlID(bounds.GetHashCode(), FocusType.Keyboard);    
			TextEditor editor = (TextEditor)GUIUtility.GetStateObject(typeof(TextEditor), controlID -1);
			
			//set background of all textfield transparent
			GUI.backgroundColor = new Color(0f, 0f, 0f, 0f);    
			
			//backup selection to remake it after process
			int backupPos = editor.pos;
			int backupSelPos = editor.selectPos;
			
			//get last position in text
			editor.MoveTextEnd();
			int endpos = editor.pos;
			
			Random.seed = 123;
			
			//draw textfield with color on top of text area
			editor.MoveTextStart();        
			while (editor.pos != endpos)
			{
				editor.SelectToStartOfNextWord();
				string wordtext = editor.SelectedText;    
				
				//set word color
				//			GUI.contentColor = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
				GUI.contentColor = Color.black;
				//draw each word with a random color
				Vector2 pixelselpos = style.GetCursorPixelPosition(editor.position, editor.content, editor.selectPos);
				Vector2 pixelpos = style.GetCursorPixelPosition(editor.position, editor.content, editor.pos);
				GUI.TextField(new Rect(pixelselpos.x - style.border.left + 10, pixelselpos.y - style.border.top + 10, pixelpos.x, pixelpos.y), wordtext);
				
				editor.MoveToStartOfNextWord();
			}

			//Reposition selection
			Vector2 bkpixelselpos = style.GetCursorPixelPosition(editor.position, editor.content, backupSelPos);    
			editor.MoveCursorToPosition(bkpixelselpos);    
			
			//Remake selection
			Vector2 bkpixelpos = style.GetCursorPixelPosition(editor.position, editor.content, backupPos);    
			editor.SelectToPosition(bkpixelpos);    
			

			GUI.enabled = true;


			
			GUI.backgroundColor = Color.clear;
			if(GUI.Button(bounds, ""))
			{
				showDialog = false;
			}


			//Reset color
			GUI.color = backupColor;
			GUI.contentColor = backupContentColor;
			GUI.backgroundColor = backupBackgroundColor;

		}

	}

	

	void FadeToBlack ()
	{
		// Lerp the colour of the texture between itself and black.
		
		FadeOutImg.color = Color.Lerp(FadeOutImg.color, Color.black, fadeSpeed * Time.deltaTime);
	}
	
	
	
	public void FadeOut ()
	{
		// Start fading towards black.
		FadeToBlack();
		//		Debug.Log("------------> is turn new scene");
		// If the screen is almost black...
		if(FadeOutImg.color.a >= 0.95f)
		{
			isEnded = true;
			shouldFadeOut = false;
			//load nextScenario
			Application.LoadLevel("LevelScene");
		}
		
	}

}
