﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelScript : MonoBehaviour {

	public UnityEngine.UI.Image menuContainer;

	public static string username;
	public UnityEngine.UI.Text wellcomeUser;
	public UnityEngine.UI.Text scenrarioText1;
	public UnityEngine.UI.Text scenrarioText2;
	public UnityEngine.UI.Text scenrarioText3;
	public UnityEngine.UI.Text scenrarioText4;
	public UnityEngine.UI.Text scenrarioText5;
	public UnityEngine.UI.Text scenrarioText6;

	public UnityEngine.UI.Button scenarioBtn1;
	public UnityEngine.UI.Button scenarioBtn2;
	public UnityEngine.UI.Button scenarioBtn3;
	public UnityEngine.UI.Button scenarioBtn4;
	public UnityEngine.UI.Button scenarioBtn5;
	public UnityEngine.UI.Button scenarioBtn6;


	void Start () {

		scenarioBtn1.image.sprite = Sprite.Create( (Texture2D)(Resources.Load ("images/scenario1")) , new Rect(0,0,342,421), new Vector2(0,0));
		scenarioBtn2.image.sprite = Sprite.Create( (Texture2D)(Resources.Load ("images/scenario2")) , new Rect(0,0,342,421), new Vector2(0,0));
		scenarioBtn3.image.sprite = Sprite.Create( (Texture2D)(Resources.Load ("images/scenario3")) , new Rect(0,0,342,421), new Vector2(0,0));
		scenarioBtn4.image.sprite = Sprite.Create( (Texture2D)(Resources.Load ("images/scenario4")) , new Rect(0,0,342,421), new Vector2(0,0));
		scenarioBtn5.image.sprite = Sprite.Create( (Texture2D)(Resources.Load ("images/scenario5")) , new Rect(0,0,342,421), new Vector2(0,0));
		scenarioBtn6.image.sprite = Sprite.Create( (Texture2D)(Resources.Load ("images/scenario6")) , new Rect(0,0,342,421), new Vector2(0,0));
		Random.seed = 123;
		
		scenarioBtn1.transform.Rotate(new Vector3(0,0,Random.Range(-5f, 5f)));
		scenarioBtn2.transform.Rotate(new Vector3(0,0,Random.Range(-5f, 5f)));
		scenarioBtn3.transform.Rotate(new Vector3(0,0,Random.Range(-5f, 5f)));
		scenarioBtn4.transform.Rotate(new Vector3(0,0,Random.Range(-5f, 5f)));
		scenarioBtn5.transform.Rotate(new Vector3(0,0,Random.Range(-5f, 5f)));
		scenarioBtn6.transform.Rotate(new Vector3(0,0,Random.Range(-5f, 5f)));
	}
	
	
	// Update is called once per frame
	void Update () {
		wellcomeUser.text = "welcome: " + username;


	}

	

	void OnGUI() {

	}

	public void scenarioButtonClick(int id){
		Debug.Log("Handle Click on Scenario :" + id);
		string sceneName = "ScenarioScene" + id;
		Application.LoadLevel(sceneName);
	}
	

}
