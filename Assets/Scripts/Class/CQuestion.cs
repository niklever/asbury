﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CQuestion {
	public int _id;
	public int _branch;
	public string _object;
	public string _text;
	public float _timewait;
	public float _timeshow;
	public List<CAction> _actions = new List<CAction>();
	public List<int> _from = new List<int>();
	public List<int> _to = new List<int>();
	public CQuestion(){
		_from = new List<int>();
		_to = new List<int>();
	}
}

