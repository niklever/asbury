﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class CProcessQuestion : MonoBehaviour {

	private static bool shouldFadeOut = false;
	private bool isEnded = false;
	public int currentScenario = 1;
	public float fadeSpeed = 1.5f;
	public UnityEngine.UI.Image FadeOutImg;


	public UnityEngine.UI.Image group1;
	public UnityEngine.UI.Button group1_btn1;
	public UnityEngine.UI.Button group1_close1;


	public UnityEngine.UI.Image coaching_group;
	public UnityEngine.UI.Text  coaching_group_text;

	Sprite minimize_dialog,maximize_dialog;
	Vector3 maximize_group1_pos,minimize_group1_pos,maximize_group2_pos,minimize_group2_pos,maximize_group3_pos,minimize_group3_pos,maximize_group4_pos,minimize_group4_pos;
	public UnityEngine.UI.Text  group1_btn1_text;
	public UnityEngine.UI.Image group2;
	public UnityEngine.UI.Button group2_btn1;
	public UnityEngine.UI.Text  group2_btn1_text;
	public UnityEngine.UI.Button group2_btn2;
	public UnityEngine.UI.Text  group2_btn2_text;
	public UnityEngine.UI.Image group3;
	public UnityEngine.UI.Button group3_btn1;
	public UnityEngine.UI.Text  group3_btn1_text;
	public UnityEngine.UI.Button group3_btn2;
	public UnityEngine.UI.Text  group3_btn2_text;
	public UnityEngine.UI.Button group3_btn3;
	public UnityEngine.UI.Text  group3_btn3_text;
	public UnityEngine.UI.Image group4;
	public UnityEngine.UI.Button group4_btn1;
	public UnityEngine.UI.Text  group4_btn1_text;
	public UnityEngine.UI.Button group4_btn2;
	public UnityEngine.UI.Text  group4_btn2_text;
	public UnityEngine.UI.Button group4_btn3;
	public UnityEngine.UI.Text  group4_btn3_text;
	public UnityEngine.UI.Button group4_btn4;
	public UnityEngine.UI.Text  group4_btn4_text;
	private List<CQuestion> _lstChosingTracking;
	bool isMinimizeAnswer;
	CQuestion currQuestion;
	private OPT_TYPE opt_type;

	private CQuestion currentQuestion;
	private List<CQuestion> questions;
	private CProcessAction processAction;
	// Use this for initialization
	void Start () {
		opt_type = 0;
		processAction = GameObject.Find("Canvas").gameObject.GetComponent<CProcessAction>();
		_lstChosingTracking = new List<CQuestion>();
		maximize_group1_pos = group1.GetComponent<RectTransform>().localPosition;
		minimize_group1_pos = new Vector3(maximize_group1_pos.x ,maximize_group1_pos.y - 50);

		maximize_group2_pos = group2.GetComponent<RectTransform>().localPosition;
		minimize_group2_pos = new Vector3(maximize_group2_pos.x ,maximize_group2_pos.y - 70);

		maximize_group3_pos = group3.GetComponent<RectTransform>().localPosition;
		minimize_group3_pos = new Vector3(maximize_group3_pos.x ,maximize_group3_pos.y - 70);

		maximize_group4_pos = group4.GetComponent<RectTransform>().localPosition;
		minimize_group4_pos = new Vector3(maximize_group4_pos.x ,maximize_group4_pos.y - 70);


		Texture2D texture = (Texture2D)(Resources.Load ("images/minimize_dialog"));
		minimize_dialog = Sprite.Create( texture , new Rect(0,0,texture.width,texture.height), new Vector2(0,0));
		texture = (Texture2D)(Resources.Load ("images/dialogue"));
		maximize_dialog = Sprite.Create( texture , new Rect(0,0,texture.width,texture.height), new Vector2(0,0));
	}
	public void setListQuestion(List<CQuestion> lstQuestion){
		questions = new List<CQuestion>();
		questions = lstQuestion;
		if(questions != null  && questions.Count>0){
			List<int> lstOpt = new List<int>();
			lstOpt.Add(1);
			doProcess(lstOpt);
		}
	}

	// Update is called once per frame
	void Update ()
	{
		if(shouldFadeOut && !isEnded)
		{
			FadeOut();
		}
	}

	private CQuestion getQuestionById(int id){
		foreach(CQuestion cQuestion in questions)
		{
			if(cQuestion._id == id)
				return cQuestion;
		}
		return null;
	}

	private CQuestion getQuestionByNameAndBranch(string name, int branch){
		foreach(CQuestion cQuestion in questions)
		{
			if(cQuestion._object.Equals(name) && cQuestion._branch == branch)
				return cQuestion;
		}
		return null;
	}

	private void doAction(List<CAction> actions){
		for(int i =0; i < actions.Count; i++){
			if(processAction != null){
				processAction.playAction(actions[i]);
			}
		}
	}
	private void doProcess(List<int> lstOpt){
		opt_type = (OPT_TYPE) lstOpt.Count;
		CQuestion question = null;
		switch(opt_type)
		{
		case OPT_TYPE.ONE:
			// set opt contents
			question = getQuestionById(lstOpt[0]);
			group1.gameObject.SetActive(true);
			group2.gameObject.SetActive(false);
			group3.gameObject.SetActive(false);
			group4.gameObject.SetActive(false);



			group1_btn1.name = question._id.ToString();
			group1_btn1_text.text = question._text.ToString();

			if(question._actions != null && question._actions.Count > 0){
				doAction(question._actions);
			}
				break;
		case OPT_TYPE.TWO:
			group1.gameObject.SetActive(true);
			group2.gameObject.SetActive(true);
			group3.gameObject.SetActive(false);
			group4.gameObject.SetActive(false);

			// set opt contents
			question = getQuestionById(lstOpt[0]);
			group2_btn1.name = question._id.ToString();
			group2_btn1_text.text = question._text.ToString();
			if(question._actions != null && question._actions.Count > 0){
				doAction(question._actions);
			}

			question = getQuestionById(lstOpt[1]);
			group2_btn2.name = question._id.ToString();
			group2_btn2_text.text = question._text.ToString();
			if(question._actions != null && question._actions.Count > 0){
				doAction(question._actions);
			}
			break;
		case OPT_TYPE.THREE:
			group1.gameObject.SetActive(true);
			group2.gameObject.SetActive(false);
			group3.gameObject.SetActive(true);
			group4.gameObject.SetActive(false);

			// set opt contents
			question = getQuestionById(lstOpt[0]);
			group3_btn1.name = question._id.ToString();
			group3_btn1_text.text = question._text.ToString();
			if(question._actions != null && question._actions.Count > 0){
				doAction(question._actions);
			}

			question = getQuestionById(lstOpt[1]);
			group3_btn2.name = question._id.ToString();
			group3_btn2_text.text = question._text.ToString();
			if(question._actions != null && question._actions.Count > 0){
				doAction(question._actions);
			}

			question = getQuestionById(lstOpt[2]);
			group3_btn3.name = question._id.ToString();
			group3_btn3_text.text = question._text.ToString();
			if(question._actions != null && question._actions.Count > 0){
				doAction(question._actions);
			}

			break;
		case OPT_TYPE.FOUR:
			group1.gameObject.SetActive(true);
			group2.gameObject.SetActive(false);
			group3.gameObject.SetActive(false);
			group4.gameObject.SetActive(true);

			// set opt contents
			question = getQuestionById(lstOpt[0]);
			group4_btn1.name = question._id.ToString();
			group4_btn1_text.text = question._text.ToString();
			if(question._actions != null && question._actions.Count > 0){
				doAction(question._actions);
			}

			question = getQuestionById(lstOpt[1]);
			group4_btn2.name = question._id.ToString();
			group4_btn2_text.text = question._text.ToString();
			if(question._actions != null && question._actions.Count > 0){
				doAction(question._actions);
			}

			question = getQuestionById(lstOpt[2]);
			group4_btn3.name = question._id.ToString();
			group4_btn3_text.text = question._text.ToString();
			if(question._actions != null && question._actions.Count > 0){
				doAction(question._actions);
			}

			question = getQuestionById(lstOpt[3]);
			group4_btn4.name = question._id.ToString();
			group4_btn4_text.text = question._text.ToString();
			if(question._actions != null && question._actions.Count > 0){
				doAction(question._actions);
			}

			break;
		default:
			group1.gameObject.SetActive(false);
			group2.gameObject.SetActive(false);
			group3.gameObject.SetActive(false);
			group4.gameObject.SetActive(false);
			break;
		}
	}
	public void minimizeAnswer()
	{
		Debug.Log("minimizeAnswer");
		if(isMinimizeAnswer == false)
		{
			isMinimizeAnswer = true;
			group1_close1.gameObject.SetActive(false);
			group1_btn1_text.gameObject.SetActive(false);
			group1_btn1.GetComponent<RectTransform>().sizeDelta = new Vector2(800,50);
			group1_btn1.image.sprite = minimize_dialog;

			group1.GetComponent<RectTransform>().localPosition = minimize_group1_pos;
			group2.GetComponent<RectTransform>().localPosition = minimize_group2_pos;
			group3.GetComponent<RectTransform>().localPosition = minimize_group3_pos;
			group4.GetComponent<RectTransform>().localPosition = minimize_group4_pos;
			



		}else {
			isMinimizeAnswer = false;
			group1_close1.gameObject.SetActive(true);
			group1_btn1_text.gameObject.SetActive(true);
			group1_btn1.GetComponent<RectTransform>().sizeDelta = new Vector2(800,110);
			group1_btn1.image.sprite = maximize_dialog;

			group1.GetComponent<RectTransform>().localPosition = maximize_group1_pos;
			group2.GetComponent<RectTransform>().localPosition = maximize_group2_pos;
			group3.GetComponent<RectTransform>().localPosition = maximize_group3_pos;
			group4.GetComponent<RectTransform>().localPosition = maximize_group4_pos;

		}

	}
	public void onGroupOption_Click(int tag)
	{

		Debug.Log("onGroupOption_Click");
		if(isMinimizeAnswer == true && tag == 11)
		{
			minimizeAnswer();
			return;
		}else {
			isMinimizeAnswer = true;
			minimizeAnswer();
		}

		switch(tag)
		{
		case 11:
			currQuestion = getQuestionById(int.Parse(group1_btn1.name));
			break;
		case 21:
			currQuestion = getQuestionById(int.Parse(group2_btn1.name));
			break;
		case 22:
			currQuestion = getQuestionById(int.Parse(group2_btn2.name));
			break;
		case 31:
			currQuestion = getQuestionById(int.Parse(group3_btn1.name));
			break;
		case 32:
			currQuestion = getQuestionById(int.Parse(group3_btn2.name));
			break;
		case 33:
			currQuestion = getQuestionById(int.Parse(group3_btn3.name));
			break;
		case 41:
			currQuestion = getQuestionById(int.Parse(group4_btn1.name));
			break;
		case 42:
			currQuestion = getQuestionById(int.Parse(group4_btn2.name));
			break;
		case 43:
			currQuestion = getQuestionById(int.Parse(group4_btn3.name));
			break;
		case 44:
			currQuestion = getQuestionById(int.Parse(group4_btn4.name));
			break;
		default:
			currQuestion = getQuestionById(int.Parse(group1_btn1.name));
			break;
		}

		if(currQuestion != null)
		{
			Debug.Log ("currQuestion._branch " +currQuestion._branch );
			if(currQuestion._branch > 0)	// B1 opt
			{
				_lstChosingTracking.Add(currQuestion);
				
			}

			List<int> lstOpt = currQuestion._to;
			// stand on the last question!!!
			if(lstOpt.Count == 0 
			   || ( currentScenario == 3 &&  (currQuestion._id == 27 
			                               || currQuestion._id == 29
			                               || currQuestion._id == 19|| currQuestion._id == 39|| currQuestion._id == 33
			                               || currQuestion._id == 41))

			   )
			{
				Debug.Log ("currQuestion._id " +currQuestion._id  + ", " +  _lstChosingTracking.Count );
				int count = _lstChosingTracking.Count;
				CQuestion lastChose =null,preChoose = null;
				if(count >= 1)
				{
					lastChose = _lstChosingTracking[count -1];
					if(currentScenario == 1)
					{
						if(count >= 2)
						{
							preChoose = (_lstChosingTracking[count -2]);
							Debug.Log ("_lstChosingTracking[count -2]" + (_lstChosingTracking[count -2])._branch);
						}
						

						Debug.Log ("_lstChosingTracking[count -1]" + (_lstChosingTracking[count -1])._branch);
						// choose C
						if(preChoose._branch == 3 )
						{
							// then choose B1
							if(lastChose._branch == 2)
							{
								showCoachingWithText(getQuestionByNameAndBranch("Coaching",1)._text);
								
							}
							// then choose A1
							else {
								showCoachingWithText(getQuestionByNameAndBranch("Coaching",2)._text);
							}
						}
						// choose other opt : A,B,D
						else {
							showCoachingWithText(getQuestionByNameAndBranch("Coaching",2)._text);
						}
					}else if(currentScenario == 2)
					{
						// then choose A
						if(lastChose._branch == 1)
						{
							showCoachingWithText(getQuestionByNameAndBranch("Coaching",2)._text);
							
						}
						// choose B
						else if(lastChose._branch == 2)
						{
							showCoachingWithText(getQuestionByNameAndBranch("Coaching",1)._text);
						}
						// choose C,D
						else {
							showCoachingWithText(getQuestionByNameAndBranch("Coaching",3)._text);
						}
					}else if(currentScenario == 3)
					{
						if(currQuestion._id == 27)
						{
							showCoachingWithText(getQuestionByNameAndBranch("Coaching",1)._text);
						}else if(currQuestion._id == 29)
						{
							showCoachingWithText(getQuestionByNameAndBranch("Coaching",2)._text);

						}else if(currQuestion._id == 39 || currQuestion._id == 19|| currQuestion._id == 33)
						{
							showCoachingWithText(getQuestionByNameAndBranch("Coaching",3)._text);
						}else if(currQuestion._id == 41)
						{
							showCoachingWithText(getQuestionByNameAndBranch("Coaching",4)._text);
						}
					}else if(currentScenario == 4)
					{
						showCoachingWithText(getQuestionByNameAndBranch("Coaching",1)._text);
					}else if(currentScenario == 5)
					{
						// then choose A
						if(lastChose._branch == 1)
						{
							showCoachingWithText(getQuestionByNameAndBranch("Coaching",1)._text);
							
						}else{
							CQuestion firstChose = _lstChosingTracking[0];
							if(firstChose._branch == 2)
							{
								showCoachingWithText(getQuestionByNameAndBranch("Coaching",2)._text);
							}
							else
								showCoachingWithText(getQuestionByNameAndBranch("Coaching",3)._text);
						}
					}else if(currentScenario == 6)
					{
						// choose A
						if(lastChose._branch == 1)
						{
							showCoachingWithText(getQuestionByNameAndBranch("Coaching",2)._text);
						}// choose B
						else if(lastChose._branch == 2)
						{
							showCoachingWithText(getQuestionByNameAndBranch("Coaching",3)._text);
						}else {
							showCoachingWithText(getQuestionByNameAndBranch("Coaching",1)._text);
						}
					}
					
				}

			}else {
				bool shouldLoop = false;
				do{
					shouldLoop = false;
					doProcess(lstOpt);
					// set opt contents
					CQuestion question = getQuestionById(lstOpt[0]);
					if(question!= null)
					{
						if(lstOpt.Count == 1)
						{
							if(question._text.Equals(""))
							{
								/*handle action*/
								//							doProcess(question._to);
								lstOpt = question._to;
								shouldLoop = true;
								continue;
								//							return;
							}
						}

						if(question._to.Count > 1 && lstOpt.Count == 1)
						{
							/*show option imediatelly */
//							doProcess(question._to);
							lstOpt = question._to;
							shouldLoop = true;
						}
					}
				}while(shouldLoop);



			}

		}
	}

	private void showCoachingWithText(string text)
	{
		coaching_group.gameObject.SetActive(true);
		coaching_group_text.text = text;
	}

	public void closeAndGoNextScenario(int scenario)
	{
//		coaching_group.gameObject.SetActive(false);
		FadeOutImg.gameObject.SetActive(true);
		FadeOutImg.color = Color.clear;
		shouldFadeOut = true;
	}


	
	void FadeToBlack ()
	{
		// Lerp the colour of the texture between itself and black.

		FadeOutImg.color = Color.Lerp(FadeOutImg.color, Color.black, fadeSpeed * Time.deltaTime);
	}
	
	
	
	public void FadeOut ()
	{
		// Start fading towards black.
		FadeToBlack();
		//		Debug.Log("------------> is turn new scene");
		// If the screen is almost black...
		if(FadeOutImg.color.a >= 0.95f)
		{
			isEnded = true;
			shouldFadeOut = false;
			//load nextScenario
			if(currentScenario < 6)
			{
				string sceneName = "ScenarioScene"+ (currentScenario + 1);
				Application.LoadLevel(sceneName);
			}else {
				Application.LoadLevel("LevelScene");
			}
			

		}

	}

}
