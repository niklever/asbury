﻿using UnityEngine;
using System.Collections;

public class CProcessAction : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void playAction(CAction action){
		if(action._function.Equals("playAnimation")){
			playAnimation(action);
		}
	}
	private void playAnimation(CAction action){
		GKIMLog.Log("-------- playAnimation ---- "+ action._object);
		GameObject obj = GameObject.Find(action._object);
		if(obj != null){
			PlayAnimator playAnimator  = obj.gameObject.GetComponent<PlayAnimator>();
			if(playAnimator != null){
				playAnimator.play(action._key);
			}else{
				GKIMLog.Log("-------- playAnimation ---- don't get Component PlayAnimator ");
			}
		}else{
			GKIMLog.Log("-------- playAnimation ---- don't find "+ action._object);
		}
	}
}
