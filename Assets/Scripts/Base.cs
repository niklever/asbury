﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using eDriven.Gui.Components;
using eDriven.Gui.Containers;
using eDriven.Gui.Cursor;
using eDriven.Gui.Layout;
using eDriven.Gui.Plugins;
using eDriven.Gui.Styles;
using Debug=UnityEngine.Debug;
using eDriven.Networking.Rpc;
using eDriven.Networking.Rpc.Loaders;
using eDriven.Core.Caching;
using eDriven.Core.Util;
using eDriven.Networking.Util;

public enum BadgeAward { APPLE, DISTRICT_AWARD, FLOWER, HEART, PENCIL,STAR,TEACHER_1,THANKYOUNOTE,NONE };

public enum OPT_TYPE { ONE = 1, TWO, THREE, FOUR };

public enum SCENARIO_ITEM { ONE = 1, TWO, THREE, FOUR, FIVE, SIX};

public class Base {
	public static readonly string BASE_URL = "http://http://dev.asbury.gkxim.com/";
}
public class CustomTextureLoader : IAsyncLoader<Texture>
{
	
	public HttpConnector Connector = new HttpConnector
	{
		ConcurencyMode = ConcurencyMode.Multiple,
		ProcessingMode = ProcessingMode.Async,
		ResponseMode = ResponseMode.WWW
	};
	
	readonly static Cache<string, Texture> ImageCache = new Cache<string, Texture>();
	
	private bool _cache;
	
//	private SlidingButton _btn;
	public bool Cache
	{
		get
		{
			return _cache;
		}
		set
		{
			if (value == _cache) 
				return;
			
			if (!_cache)
				ImageCache.Clear();
			
			_cache = value;
		}
	}
	public void LoadImage(string id, AsyncLoaderCallback<Texture> callback)
	{
//		_btn = btn;
		Load(id,callback);
	}
	/*
	public void LoadImage(SlidingButton btn,string id, AsyncLoaderCallback<Texture> callback)
	{
		_btn = btn;
		Load(id,callback);
	}
	*/
	public void Load(string id, AsyncLoaderCallback<Texture> callback)
	{
		Texture cashedTexture = _cache ? ImageCache.Get(id) : null;
		if (null != cashedTexture)
		{
			callback(cashedTexture);
		}
		else
		{
			try {
				Connector.Send(
					new WebRequest(id),
					new Responder(
					delegate(object data)
					{
					try {
						WWW www = (WWW)data;
						if (www == null || www.error != null || www.bytesDownloaded == 0) {
							//assign default 
//							if(_btn != null)
//							{
//								_btn.Texture = (Texture)Resources.Load("images/temp/no_image");
//							}
							callback((Texture)Resources.Load("images/temp/no_image"));
						}else{
							var texture = new Texture2D(150, 200);
							www.LoadImageIntoTexture(texture);
							if (_cache)
								ImageCache.Put(id, texture);
							callback(texture);
						}
					}catch(Exception ex) {
//						if(_btn != null)
//						{
//							_btn.Texture = (Texture)Resources.Load("images/temp/no_image");
//						}
						callback((Texture)Resources.Load("images/temp/no_image"));
					}
				},
				delegate(object data)
				{
					//Logger.Log("Image loading failed: " + id);
//					if(_btn != null)
//					{
//						_btn.Texture = (Texture)Resources.Load("images/temp/no_image");
//					}
					callback((Texture)Resources.Load("images/temp/no_image"));
					//				throw new Exception("Loading error:\n\n" + data);
				}
				)
					);
			}catch(Exception e) {
//				if(_btn != null)
//				{
//					_btn.Texture = (Texture)Resources.Load("images/temp/no_image");
//				}
				callback((Texture)Resources.Load("images/temp/no_image"));
			}
			
			
		}
	}
}
/*
public class LoadingButton : SlidingButton
{
	
	public void Update(){
		this.Rotation += (Time.deltaTime * 100);
	}
	
	
}

public class CustomButton : SlidingButton
{
	private bool _shouldShowLoading;
	private LoadingButton _loadingImgs;
	private Box _parent;
	public void loadBackgroundImg(string path,LoadingButton _loading, Box parent)
	{
		this._parent = parent;
		this._loadingImgs = _loading;
		_shouldShowLoading = true;
		this.Texture = (Texture)Resources.Load("images/temp/empty");
		string _imgUrl = LoginScript.ResourcePath + "images/" + path;
		CustomTextureLoader ImageLoader = new CustomTextureLoader
		{
			Cache = false,
		}; // loads textures
		ImageLoader.LoadImage(this,_imgUrl, delegate(Texture texture)
		                      
		                      {
			this._shouldShowLoading = false;
			if(texture == null)
			{
				
				this.Texture = (Texture)Resources.Load("images/temp/no_image");
				
			}else
				this.Texture = texture;
		}
		
		);
		
		
		
		
	}
	
	public void Update(){
		
		if(this._loadingImgs!= null)
		{
			
			if(this._shouldShowLoading) {
				this._loadingImgs.Update();
				
			}else {
				this._parent.RemoveChild(this._loadingImgs);
				this._loadingImgs = null;
			}
		}
		
	}
	
	
}
*/
public class Student {

	public string name;
	public string avatar;
	public string description;

	public Student(){
		name = "";
		avatar = "";
		description = "";
	}

	public Student(string _name, string _avatar, string _desription){
		name = _name;
		avatar = _avatar;
		description = _desription;


	}
}

public class BadgeAwardItem {

	public string note;
	public BadgeAward type;

	public BadgeAwardItem(){
		note = null;
		type = BadgeAward.NONE;
	}
	
	public BadgeAwardItem(BadgeAward _type, string _note){
		type = _type;
		note = _note;
	}
}

