using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Config : MonoBehaviour {
	public static string NAME_GUI_SKIN_MAIN_WELCOME = "Main_Welcome";
	public static string NAME_GUI_SKIN_MAIN_SELECT = "Main_Select";
	public static string NAME_GUI_SKIN_MAIN_SCENARIO_TITLE = "Main_Scenario_Title";

	public static string NAME_GUI_SKIN_GAME_TITTLE = "Game_Title";
	public static string NAME_GUI_SKIN_GAME_COACH_DETAIL = "Game_Coach_Detail";
	public static string NAME_GUI_SKIN_GAME_RESOURCES_STUDENT_NAME = "Game_Resources_Student_Name";
	public static string NAME_GUI_SKIN_GAME_RESOURCES_STUDENT_DESC = "Game_Resources_Student_Desc";
	public static string NAME_GUI_SKIN_GAME_DIALOGUE_TEXT = "Dialogue_text";


	
	public static string SCENE_SPLASH = "MainScreen";
	public static string SCENE_MAIN = "MainScreen";
	public static string SCENE_GAME = "GameScreen";


	public static GUIStyle fontStyle = null;
 	
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public static string GetStreamingAssetsPath()
	{
		string path;
		#if UNITY_EDITOR
			path = "file:" + Application.dataPath + "/StreamingAssets/";
		#elif UNITY_ANDROID
			path = "jar:file://"+ Application.dataPath + "!/assets/";
		#elif UNITY_IOS
			path = "file:" + Application.dataPath + "/Raw/";
		#else
			//Desktop (Mac OS or Windows)
			path = Application.dataPath + "/StreamingAssets/";
		#endif
		
		return path;
	}
}
