﻿using UnityEngine;
using System.Collections;

public class PlayAnimator : MonoBehaviour {

	Animator anim;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
//		if(Input.GetKeyDown(KeyCode.Space) && isTest){
//			int hash = Animator.StringToHash("raise_hand");
//			anim.SetTrigger(hash);
//		}
	}
	public void play(string name){
		GKIMLog.Log(" PlayAnimator ---- "+ name);
		int hash = Animator.StringToHash(name);
		anim.SetTrigger(hash);
	}
}
