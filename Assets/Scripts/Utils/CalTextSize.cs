using UnityEngine;
using System.Collections;

public class CalTextSize : MonoBehaviour {
	
	public GUISkin selectEffectSkin;
	
	public static Vector2 getSizeText(GUISkin skin, string text, string nameSkin){
		Vector2 v2 = skin.GetStyle(nameSkin).CalcSize(new GUIContent(text));
		return v2;
	}
}