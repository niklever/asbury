using UnityEngine;
using System.Collections;

public class GKIMLog : MonoBehaviour {

	// Use this for initialization
	const bool isLog= true;
	const bool isShow = false;
	static string mLog="";
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnGUI(){
		if(mLog !=""){
			GUI.Label(new Rect(10, 10, Screen.width - 10, 50),mLog);
		}
	}
	public static void Log(string str){
		if(isLog){
			Debug.Log("GKIMLog"+Time.deltaTime+">>" + str);
		}

	}
	public static void drawLog(string str){
		if(isShow){
			mLog += str + "\n";
		}
	}
}
