using UnityEngine;
using System.Collections;
 
public class CalculatorRatio : MonoBehaviour
{
	private const string TAG ="CalculatorRatio";
	private const float ScreenWidth = 1024.0f;
	private const float ScreenHeight = 768.0f;
	private static float scale;

	public CalculatorRatio(){
		float scaleW = Screen.width/ScreenWidth;
		float scaleH = Screen.height/ScreenHeight;
		scale = scaleW > scaleH ? scaleH : scaleW;
		GKIMLog.Log(TAG + " scale:"+ scale);

	}
	public static float ratioWidth(float width){
		return width/ScreenWidth;
	}
	public static float ratioHeight(float height){
		return height/ScreenHeight;
	}
	public static float ScaleWidth(float width){
		return (Screen.width * width) / ScreenWidth;
	}
	
	public static float ScaleHeight(float height){
		return (Screen.height * height) / ScreenHeight;
	}

	public static float ScaleImageW(float width){
		return width*scale;
	}
	public static float ScaleImageH(float height){
		return height*scale;
	}
}