﻿using UnityEngine;
using System.Collections;

public class AutoRotate : MonoBehaviour {

	// Use this for initialization
	public float speed = 0.1F;
	private Vector2 offset = Vector2.zero;
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
//		if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved) {
//			Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
//			transform.Translate(-touchDeltaPosition.x * speed, -touchDeltaPosition.y * speed, 0);
//		}
		transform.Rotate(Vector3.forward * Time.deltaTime * 100);
	}
	
	void SwingOpen()
	{   
		Quaternion newRotation = Quaternion.AngleAxis(90, Vector3.up);
		transform.rotation= Quaternion.Slerp(transform.rotation, newRotation, .05f);      
	}
}
