﻿using UnityEngine;
using System.Collections;

public class PlayAnimation : MonoBehaviour {

	// Use this for initialization
	void Start () {
		animation.wrapMode = WrapMode.Loop;
//		setAnimation("raise_hand", true);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void setAnimation(string name, bool loop){
		if(name != null){
			animation.wrapMode = loop ? WrapMode.Loop : WrapMode.Once;
//			animation[name].speed = 1f;
			animation.Play(name);
		}
	}

	public void pauseAnimtion(string name){
		animation[name].speed = 0;
	}
}
