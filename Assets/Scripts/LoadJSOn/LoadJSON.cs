using UnityEngine;
using LitJson;
using System;
using System.Collections;
using System.Collections.Generic;

public class LoadJSON : MonoBehaviour
{
	private const string TAG ="LoadJSON";
	public int scenario;
    IEnumerator Start()
    {
        //Load JSON data from a URL
		string filePath = Config.GetStreamingAssetsPath() +"scenario"+ scenario + ".json";
		Debug.Log("Loaded filePath:"+ filePath);

		WWW www = new WWW(filePath);
		//Load the data and yield (wait) till it's ready before we continue executing the rest of this method.
        yield return www;
        if (www.error == null)
        {
            //Sucessfully loaded the JSON string
            GKIMLog.Log("Loaded following JSON string" + www.data);
			GKIMLog.drawLog("Loaded following JSON string");
            //Process books found in JSON file
			readQuestion(www.data);
        }else{
            Debug.LogError("ERROR: " + www.error);
			GKIMLog.drawLog("LoadJSON ERROR:"+www.error);
        }

    }
	protected void readQuestion(string jsonString){
		JsonData jsonData = JsonMapper.ToObject(jsonString);
		List<CQuestion> _questions = new List<CQuestion>();
		CQuestion question = null;
		GKIMLog.Log("jsonData question Count:" + jsonData["question"].Count);

		for(int i=0; i < jsonData["question"].Count; i++){
			question = new CQuestion();
			question._object = jsonData["question"][i]["object"].ToString();
			question._branch = int.Parse(jsonData["question"][i]["branch"].ToString());

			question._id = int.Parse(jsonData["question"][i]["id"].ToString());
			question._text = jsonData["question"][i]["text"].ToString();
			question._timewait = float.Parse(jsonData["question"][i]["timewait"].ToString());
			question._timeshow = float.Parse(jsonData["question"][i]["timeshow"].ToString());
			if(jsonData["question"][i]["from"] != null && jsonData["question"][i]["from"].Count >0){
				for(int j=0; j < jsonData["question"][i]["from"].Count; j++){
					question._from.Add(int.Parse(jsonData["question"][i]["from"][j].ToString()));
				}
			}
			if(jsonData["question"][i]["to"] != null && jsonData["question"][i]["to"].Count >0){
				for(int j=0; j < jsonData["question"][i]["to"].Count; j++){
					question._to.Add(int.Parse(jsonData["question"][i]["to"][j].ToString()));
				}
			}
			int t;

//			GKIMLog.Log("load json :"+ question._id);
			if(jsonData["question"][i]["action"] != null && jsonData["question"][i]["action"].Count> 0){
				if((jsonData["question"][i]["action"])[0].IsString == false){
					for(int j=0; j < jsonData["question"][i]["action"].Count; j++){
						CAction action = new CAction();
						action._function = jsonData["question"][i]["action"][j]["function"].ToString();
						action._object = jsonData["question"][i]["action"][j]["object"].ToString();
						action._key = jsonData["question"][i]["action"][j]["key"].ToString();
						question._actions.Add(action);
					}
				}


			}
			_questions.Add(question);
		}
		GKIMLog.drawLog("Loaded readQuestion");
		CProcessQuestion  proccessQuestion = GameObject.Find("QuestionDiaLogs").gameObject.GetComponent<CProcessQuestion>();
		proccessQuestion.currentScenario = scenario;
		proccessQuestion.setListQuestion(_questions);
	}
}
