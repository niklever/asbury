using UnityEngine;
using LitJson;
using System;
using System.Collections;
using System.Collections.Generic;

public class LoadJSONLevel : MonoBehaviour
{
	private const string TAG = "LoadJSONLevel => ";
	private IList<CLevel> _LstLevel = null;
	//private MainScreen mainScreen = null;
	public IList<CLevel> getLstLevel() {
		return _LstLevel;
	}

    IEnumerator Start()
    {
        //Load JSON data from a URL
		string filePath = Config.GetStreamingAssetsPath() + "level.json";
		GKIMLog.Log(TAG + "Loaded filePath:"+ filePath);

		WWW www = new WWW(filePath);
		//Load the data and yield (wait) till it's ready before we continue executing the rest of this method.
        yield return www;
        if (www.error == null)
        {
            //Sucessfully loaded the JSON string
			GKIMLog.Log(TAG + "Loaded following JSON string" + www.data);
			_LstLevel = new List<CLevel>();
            //Process books found in JSON file
			ProcessLevel(www.data);
        }else{
            Debug.Log("ERROR: " + www.error);
        }

    }
	//Converts a JSON string into Level object
	private void ProcessLevel(string jsonString)
	{
		JsonData jsonPOI = JsonMapper.ToObject(jsonString);
		
		for(int i = 0; i<jsonPOI["level"].Count; i++)
		{	
			CLevel cLevel = new CLevel();
			cLevel._id = int.Parse(jsonPOI["level"][i]["id"].ToString());
			cLevel._day =  jsonPOI["level"][i]["day"].ToString();
			cLevel._title =  jsonPOI["level"][i]["title"].ToString();
			cLevel._image =  jsonPOI["level"][i]["image"].ToString();
			cLevel._status =  bool.Parse(jsonPOI["level"][i]["status"].ToString());
			if (_LstLevel != null) {
				_LstLevel.Add(cLevel);
			}
		}
//		mainScreen = GameObject.Find ("Main Camera").GetComponent<MainScreen> ();
//		if (mainScreen != null) {
//			mainScreen.updateData();
//		}
	}

}
