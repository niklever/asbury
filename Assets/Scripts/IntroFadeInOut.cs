﻿using UnityEngine;
using System.Collections;

public class IntroFadeInOut : MonoBehaviour
{
	public float fadeSpeed = 1.5f;          // Speed that the screen fades to and from black.
	
	public UnityEngine.UI.Image FadeOutImg;
	
	private bool sceneStarting = true;      // Whether or not the scene is still fading in.
	
	private bool isEnded = false;


	void Update ()
	{

		if(sceneStarting)
		{	// ... call the StartScene function.
			StartScene();
		}
		else if(isEnded == false)
			EndScene();
	}
	
	
	void FadeToClear ()
	{
		// Lerp the colour of the texture between itself and transparent.
		FadeOutImg.color = Color.Lerp(FadeOutImg.color, Color.clear, fadeSpeed * Time.deltaTime);
	}
	
	
	void FadeToBlack ()
	{
		// Lerp the colour of the texture between itself and black.
		FadeOutImg.color = Color.Lerp(FadeOutImg.color, Color.black, fadeSpeed * Time.deltaTime);
	}
	
	
	void StartScene ()
	{
		// Fade the texture to clear.
		FadeToClear();
		
		if(FadeOutImg.color.a <= 0.05f)
		{
			// ... set the colour to clear and disable the GUITexture.
//			FadeOutImg.color = Color.clear;
//			FadeOutImg.enabled = false;
			
			// The scene is no longer starting.
			FadeOutImg.gameObject.SetActive(true);
			sceneStarting = false;
		}
	}
	
	
	public void EndScene ()
	{
		
		// Start fading towards black.

		FadeToBlack();
		//		Debug.Log("------------> is turn new scene");
		// If the screen is almost black...
		if(FadeOutImg.color.a >= 0.95f)
		{
			// ... reload the level.
			isEnded = true;
			//		    int curLevel = Application.loadedLevel;
			Debug.Log("------------> is turn new scene : " + Application.loadedLevelName);
			// is intro screen
			if(Application.loadedLevelName.Equals("introScene"))
			{
				// load login screen
				Application.LoadLevel("loginScene");
			}
		}
	}
}

