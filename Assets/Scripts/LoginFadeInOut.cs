﻿using UnityEngine;
using System.Collections;

public class LoginFadeInOut : MonoBehaviour
{
	public float fadeSpeed = 1.5f;          // Speed that the screen fades to and from black.
	
	
	public static bool shouldFadeOut = false;  

	private bool isEnded = false;
	
	void Awake ()
	{
		// Set the texture so that it is the the size of the screen and covers it.
		this.guiTexture.pixelInset = new Rect(0f, 0f, Screen.width, Screen.height);
	}
	
	
	void Update ()
	{
		if(shouldFadeOut && !isEnded)
		{
			FadeOut();
		}
	}
	
	
	void FadeToClear ()
	{
		// Lerp the colour of the texture between itself and transparent.
		this.guiTexture.color = Color.Lerp(this.guiTexture.color, Color.clear, fadeSpeed * Time.deltaTime);
	}
	
	
	void FadeToBlack ()
	{
		// Lerp the colour of the texture between itself and black.
		this.guiTexture.color = Color.Lerp(this.guiTexture.color, Color.black, fadeSpeed * Time.deltaTime);
	}
	

	
	public void FadeOut ()
	{
		// Make sure the texture is enabled.
		this.guiTexture.enabled = true;
		
		// Start fading towards black.
		FadeToBlack();

		{
			// ... reload the level.
			isEnded = true;

			Debug.Log("------------> is turn new scene : " + Application.loadedLevelName);
			// is intro screen
			if(Application.loadedLevelName.Equals("loginScene"))
			{
				// load menu screen
				Application.LoadLevel("menuScene");
			}
		}
	}
}